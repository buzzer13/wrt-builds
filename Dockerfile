FROM debian:9.4

RUN  useradd -m ci
RUN  apt update -y \
  && apt install -y build-essential file gawk gettext git libncurses5-dev libssl-dev subversion python time unzip wget zlib1g-dev

USER ci
