.PHONY: all build menuconfig .env .feeds

.DEFAULT:
	$(MAKE) $@

all: build

build: $(BINDIR) $(DSTDIR)
	cd $(BINDIR) && cp -r $(CONFIG) $(BINS) $(DSTDIR)

menuconfig: .feeds .env
	$(MAKE) $@
	cp -f $(SRCDIR)/.config $(CONFIG)

$(BINDIR): .feeds .env
	$(MAKE) defconfig
	$(MAKE) clean
	$(MAKE)

.env: $(CONFIG) $(FILESDIR) $(PATCHDIR)
	ln -frs $(CONFIG) $(FILESDIR) $(SRCDIR)
	cp -fr $(PATCHDIR)/. $(SRCDIR)

.feeds:
	cd $(SRCDIR) && $(FEEDS) update -a && $(FEEDS) install -a

$(CONFIG):
	$(MAKE) defconfig
	cp -f $(SRCDIR)/.config $(CONFIG)

$(FILESDIR) $(PATCHDIR) $(DSTDIR):
	mkdir -p $@
