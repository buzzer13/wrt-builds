TARGET  ?= ar71xx/generic
BINS    ?= *
JOBS    ?= $(shell nproc)
VERBOSE ?= c

CFGDIR   = $(patsubst %/,%,$(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
SRCDIR   = $(PWD)/source
BUILDDIR = $(PWD)/build
DSTDIR   = $(BUILDDIR)/$(notdir $(CFGDIR))
BINDIR   = $(SRCDIR)/bin/targets/$(TARGET)

FILESDIR = $(CFGDIR)/files
PATCHDIR = $(CFGDIR)/patches
CONFIG   = $(CFGDIR)/.config

FEEDS    = ./scripts/feeds
MAKE     = make -C $(SRCDIR) -j$(JOBS) V=$(VERBOSE)
