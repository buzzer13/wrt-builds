include config/vars.mk

CONFIGS = $(filter %/, $(wildcard config/*/))

.PHONY: all clean buildclean $(CONFIGS)

.DEFAULT:
	$(MAKE) $@

all: buildclean $(CONFIGS)

clean: buildclean
	$(MAKE) $@

buildclean:
	rm -rf $(BUILDDIR)

$(CONFIGS):
	make -C $@
