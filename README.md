# WRT Builds

## Status

* **Build:** [![build status](https://gitlab.com/buzzer13/wrt-builds/badges/master/build.svg)](https://gitlab.com/buzzer13/wrt-builds/pipelines)

## What is this?

This is the **custom** router firmware build system. Currently supports following configs and devices:

- *ar71xx* (**ar71xx/generic** target with LuCI, sit tunnels, 3G/4G modems, block device, f2fs/ext4/swap FS support, [samba](https://www.samba.org/) server, [aria2](https://aria2.github.io/) and [ddns-scripts](https://openwrt.org/docs/guide-user/services/ddns/client))
	- [GL.iNet GL-AR300M/MD](https://openwrt.org/toh/hwdata/gl.inet/gl.inet_gl-ar300m)
	- [GL.iNet GL-AR750](https://openwrt.org/toh/hwdata/gl.inet/gl.inet_gl-ar750)
- *ar71xx-nostorage* (**ar71xx/generic** target with LuCI, sit tunnels, 3G/4G modems support)
	- [TP-Link TL-MR3020](https://openwrt.org/toh/tp-link/tl-mr3020)
	- [TP-Link TL-MR3040](https://openwrt.org/toh/tp-link/tl-mr3040)
	- [TP-Link TL-MR3220/3420](https://openwrt.org/toh/tp-link/tl-mr3420)
- *ar71xx-nousb* (**ar71xx/generic** target with LuCI, sit tunnels support)
	- [TP-Link TL-WR740N](https://openwrt.org/toh/tp-link/tl-wr740n)
	- [TP-Link TL-WR741ND](https://openwrt.org/toh/tp-link/tl-wr741nd)
- *mt7620* (**ramips/mt7620** target with same set of packages as for *ar71xx*)
	- [GL.iNet GL-MT300A](https://openwrt.org/toh/hwdata/gl.inet/gl.inet_gl-mt300a)

## Why?

Because it is fun to fit several non-fitting packages into firmware and remove unneeded functionality from kernel every time it is upgraded.

## Download

**WARNING: You're performing anything with your device at your own risk! I am not responsible for broken routers and configuration loss.**

You can [browse and download](https://gitlab.com/buzzer13/wrt-builds/-/jobs/artifacts/master/browse/build?job=build-firmware) single image or whole artifacts archive on Gitlab.

All finished builds are located at [GitLab Pipelines](https://gitlab.com/buzzer13/wrt-builds/pipelines?scope=finished) page.

Builds are semi-automatic. Although stable firmware is built, I do not test every build by myself. It is recommended to backup your configuration before upgrading and install [modified u-boot](https://github.com/pepe2k/u-boot_mod), if it supports your device.
